#!/usr/bin/env python
# coding: utf-8

# In[1]:


# hướng dẫn này chỉ có thể được sử dụng với IPython Notebook
get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np # để làm việc với dữ liệu số một cách hiệu quả
import matplotlib.pyplot as plt
A = .8 # biên độ của phuờng trình sóng
f = 5 # tần số của tín hiệu là 5
t = np.arange(0,1,.01) # các điểm trên trục x để vẽ đồ thị
phi = np.pi/4 # độ trễ pha pi/4
x = A*np.cos(2*np.pi*f*t + phi) # tính giá trị (biên độ) của sóng hình sin cho mỗi mẫu
plt.plot(t,x) # vẽ đồ thị hình sin
plt.axis([0,1,-1,1])# xác định tọa độ x, y
plt.xlabel('time in seconds')# Cho nhãn trục x cho biểu đồ sóng hình sin
plt.ylabel('amplitude') # Cho nhãn trục y cho biểu đồ sóng hình sin
plt.show() # hiện thị sóng hình sin


# In[2]:


A = .65 # biên độ
fs = 100 #tỷ lệ mẫu
samples = 100 # cho mẫu vật bằng 100
f = 5 # tần số của tín hiệu là 5
phi = 0 # phase in degrees
n = np.arange(samples)# trả về khoảng cách N
T = 1.0/fs # Remember to use 1.0 and not 1!
y = A*np.cos(2*np.pi*f*n*T + phi) # tính giá trị (biên độ) của sóng hình sin cho mỗi mẫu
plt.plot(y) # vẽ đồ thị hình sin
plt.axis([0,100,-1,1])# xác định tọa độ x, y
plt.xlabel('sample index')# Cho nhãn trục x cho biểu đồ sóng hình sin
plt.ylabel('amplitude')# Cho nhãn trục y cho biểu đồ sóng hình sin
plt.show() # hiển thị sóng hình sin


# In[3]:


A = .8 # biên độ
N = 100 # số lượng mẫu là 100
f = 5 # tần số của tín hiệu là 5
phi = 0 # phase in degrees
n = np.arange(N)# trả về khoảng cách N
y = A*np.cos(2*np.pi*f*n/N + phi)# tính giá trị (biên độ) của sóng hình sin cho mỗi mẫu
plt.plot(y) #vẽ đồ thị hình sin
plt.axis([0,100,-1,1])# xác định tọa độ x, y
plt.xlabel('sample index')# Cho nhãn trục x cho biểu đồ sóng hình sin
plt.ylabel('amplitude')# Cho nhãn trục y cho biểu đồ sóng hình sin
plt.show()# hiển thị sóng hình sin


# In[4]:


f = 3 # tần số của tín hiệu là 3
t = np.arange(0,1,.01)# # các điểm trên trục x để vẽ đồ thị
phi = 0 # phase in degrees
x = np.exp(1j*(2*np.pi*f*t + phi)) # phương trình sóng hình sin
xim = np.imag(x)# lấy phần ảo của phương trình
plt.figure(1)#tạo cửa sổ đồ họa mới
plt.plot(t,np.real(x))#vẽ đồ thị trả về phần thực
plt.plot(t,xim) #vẽ đồ thị trả về phần ảo
plt.axis([0,1,-1.1,1.1])# xác định tọa độ x, y
plt.xlabel('time in seconds')# Cho nhãn trục x cho biểu đồ sóng hình sin
plt.ylabel('amplitude')# Cho nhãn trục y cho biểu đồ sóng hình sin
plt.show()# hiển thị sóng hình sin


# In[5]:


f = 3 # tần số của tín hiệu là 3
N = 100 # số lượng mẫu là 100
fs = 100 # tỷ lệ mẫu
n = np.arange(N)# trả về khoảng cách N
T = 1.0/fs # Remember to use 1.0 and not 1!
t = N*T
phi = 0 # phase in degrees
x = np.exp(1j*(2*np.pi*f*n*T + phi)) # phương trình sóng hình sin
xim = np.imag(x)# lấy phần ảo của phương trình
plt.figure(1) # tạo cửa sổ đồ họa mới
plt.plot(n*T,np.real(x))#vẽ đồ thị trả về phần thực
plt.plot(n*T,xim)#vẽ đồ thị trả về phần ảo
plt.axis([0,t,-1.1,1.1])# xác định tọa độ x, y
plt.xlabel('t(seconds)')# Cho nhãn trục x cho biểu đồ sóng hình sin
plt.ylabel('amplitude')# Cho nhãn trục y cho biểu đồ sóng hình sin
plt.show()# hiển thị sóng hình sin


# In[6]:


f = 3 # tần số của tín hiệu là 3
N = 64 # số lượng mẫu là 64
n = np.arange(64)# trả về khoảng cách N
phi = 0 # phase in degrees
x = np.exp(1j*(2*np.pi*f*n/N + phi))# phương trình sóng hình sin
xim = np.imag(x)# lấy phần ảo của phương trình
plt.figure(1) # tạo cửa sổ đồ họa mới
plt.plot(n,np.real(x))#vẽ đồ thị trả về phần thực
plt.plot(n,xim)#vẽ đồ thị trả về phần ảo
plt.axis([0,samples,-1.1,1.1])# xác định tọa độ x, y
plt.xlabel('sample index')# Cho nhãn trục x cho biểu đồ sóng hình sin
plt.ylabel('amplitude')# Cho nhãn trục y cho biểu đồ sóng hình sin
plt.show() # hiển thị sóng hình sin


# In[7]:


N = 44100#số lượng mẫu là 44100
f = 440 # tần số của tín hiệu là 440
fs = 44100 # tỷ lệ mẫu
phi = 0 # phase in degrees
n = np.arange(N)
x = A*np.cos(2*np.pi*f*n/N + phi)# phương trình sóng hình sin
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write
write('sine440_1sec.wav', 44100, x)

